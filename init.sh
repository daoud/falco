#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

index=/ccagc/data/ref/hg19/stampy/hg19
hash=/ccagc/data/ref/hg19/stampy/hg19
faref=/ccagc/data/ref/iGenomes/Homo_sapiens/UCSC/hg19/Sequence/WholeGenomeFasta/genome.fa
bwaref=/ccagc/data/ref/iGenomes/Homo_sapiens/UCSC/hg19/Sequence/BWAIndex/version0.5.x/genome.fa
manifest=/ccagc/data/ref/manifests/TSACP/TruSeq_Amplicon_Cancer_Panel_Manifest_AFP1_PN15032433.txt
targetBed=/ccagc/data/ref/manifests/TSACP/TSACP.bed

dbSnp=/ccagc/data/ref/dbSNP/common_no_known_medical_impact_00-latest.f.vcf
dbNSFP=/ccagc/data/ref/dbNSFP/dbNSFP2.0b3.txt
clinvar=/ccagc/data/ref/dbSNP/clinvar_00-latest.f.vcf
cosmic=/ccagc/data/ref/cosmic/CosmicCodingMuts_v64_26032013_noLimit_wgs.f.vcf
cosmicNC=/ccagc/data/ref/cosmic/CosmicNonCodingVariants_v64_26032013_noLimit_wgs.f.vcf
locifilt=/ccagc/data/ref/dbSNP/locifilt.tsv
canonicals=/ccagc/data/ref/manifests/TSACP/canonicals.tsv

illmP5="AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
illmP7="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT"

adapt_clipper=/ccagc/lib/cutadapt/cutadapt-1.1/bin/cutadapt
qTrim=/ccagc/bin/fastq_quality_trimmer
stampy=/ccagc/lib/stampy/stampy-1.0.18/stampy.py
samtools=/ccagc/lib/samtools/samtools-0.1.18/samtools
bcftools=/ccagc/lib/samtools/samtools-0.1.18/bcftools/bcftools
#snpEff=/ccagc/lib/snpEff/snpEff_3_0a
snpEff=/ccagc/lib/snpEff/snpEff_v3_4
snpSift=/ccagc/lib/SnpSift

varscan=/ccagc/lib/VarScan/VarScan.v2.2.11.jar
java=/usr/bin/java
bwa=/ccagc/lib/bwa/bwa-0.5.9/bwa
flash=/ccagc/lib/flash/FLASH-1.2.2/flash

pairUp=$DIR/misc/pairUp.pl
pileup2cov=$DIR/misc/pileup2cov.pl
pileup2cov2=$DIR/misc/perAmpliconAnalysis.pl
addQual=$DIR/misc/addQual.R

qc2vcf=$DIR/misc/qc2vcf.pl
qcFilt=$DIR/misc/qcFilt.pl
plots=$DIR/misc/plots.R
plotsPng=$DIR/misc/plotsPng.R

attachManifest=$DIR/misc/attachManifest.pl
plots=$DIR/misc/plots.R
VcAn=$DIR/amplicon-VcAn-TSACP.sh
report=$DIR/amplicon-Report.sh

alignmentStats=$DIR/misc/alignmentStats.pl
errorRate=$DIR/misc/errorRate.pl
plotQc=$DIR/misc/plotsQC.R

export PATH=$PATH:/ccagc/lib/samtools/samtools-0.1.18/
