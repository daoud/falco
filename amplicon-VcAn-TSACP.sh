#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $DIR/init.sh

TMP=./tmp
if [ ! -e $TMP ]
then
	mkdir $TMP
fi

bam=$1
output=$2

if [ ! -e $output.vcf ] 
then
# Use Extended BAQ!
# Tweak indel settings -m and -F
echo `date` " [$$] - $samtools mpileup -Euf $faref -L 1000000 -d 1000000 -m 150 -F 0.01 -o 20 $bam | bcftools view -bvcg - > $output.bcf"
$samtools mpileup -Euf $faref -L 1000000 -d 1000000 -m 150 -F 0.01 -o 20 $bam | $bcftools view -bvcg - > $output.bcf
echo `date` " [$$] - Done!"
echo `date` " [$$] - bcftools view $output.bcf > $output.vcf"
$bcftools view $output.bcf > $output.vcf
echo `date` " [$$] - Done!"
fi

# Annotation
VcfIn=$output.vcf
VcfOut=$output.samtools.vcf

if [ ! -e $VcfOut ]
then
# Samtools annotation
echo `date` " [$$] - $java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > tmp.snpeff.vcf"
$java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > $output.snpeff.vcf
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar $snpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf"
$java -jar $snpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf
mv $output.snpeff.2.vcf $VcfOut
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf
#mv $output.snpeff.3.vcf $VcfOut
echo `date` " [$$] - Done!"
fi 

if [ ! -e $output.qc.ann.txt ]
then
echo `date` " [$$] - $pileup2cov2 $bam $faref $manifest $output $samtools"
$pileup2cov2 $bam $faref $manifest $output $samtools
echo `date` " [$$] - Done!"
fi

if [ ! -e $output.qc.ann.qual.txt ]
then
echo `date` " [$$] - RScript $addQual $output.qc.ann.txt $output.qc.ann.qual.txt $DIR/misc/func.R $locifilt"
Rscript $addQual $output.qc.ann.txt $output.qc.ann.qual.txt $DIR/misc/func.R $locifilt
echo `date` " [$$] - Done!"
fi

echo `date` " [$$] - $qcFilt $output.qc.ann.qual.txt $clinvar $cosmic $cosmicNC $output"
$qcFilt $output.qc.ann.qual.txt $clinvar $cosmic $cosmicNC $output
echo `date` " [$$] - Done!"

echo `date` " [$$] - $qc2vcf $output.qc.ann.qual.filt.txt > $output.qc.ann.filt.vcf"
$qc2vcf $output.qc.ann.qual.filt.txt > $output.qc.ann.filt.vcf
echo `date` " [$$] - Done!"

echo `date` " [$$] - $qc2vcf $output.qc.ann.qual.clinvar.txt > $output.qc.ann.clinvar.vcf"
$qc2vcf $output.qc.ann.qual.clinvar.txt > $output.qc.ann.clinvar.vcf
echo `date` " [$$] - Done!"



VcfIn=$output.qc.ann.filt.vcf
VcfOut=$output.qc.ann.filt.a.vcf

if [ ! -e $VcfOut ]
then

# qc vcf annotation
echo `date` " [$$] - $java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > tmp.snpeff.vcf"
$java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > $output.snpeff.vcf
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar $snpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf"
$java -jar $snpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf
mv $output.snpeff.2.vcf $VcfOut
echo `date` " [$$] - Done!"

#echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf
#mv $output.snpeff.3.vcf $VcfOut
#echo `date` " [$$] - Done!"
fi

VcfIn=$output.qc.ann.clinvar.vcf
VcfOut=$output.qc.ann.clinvar.a.vcf

if [ ! -e $VcfOut ]
then

# qc vcf annotation
echo `date` " [$$] - $java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > tmp.snpeff.vcf"
$java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > $output.snpeff.vcf
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar $snpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf"
$java -jar $snpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf
mv $output.snpeff.2.vcf $VcfOut
echo `date` " [$$] - Done!"

#echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf
#mv $output.snpeff.3.vcf $VcfOut
#echo `date` " [$$] - Done!"
fi
