#!/usr/bin/perl -w
#
# TODO: tiled targets give wrong length...
use strict;

my $gff = shift;
my $covtable = shift;

my $QCrep = shift || "qc.txt"; 

my %targets = ();
my %stats = ();
my %manif = ();
open M, "<$gff";
while (<M>) {
	if (/^track/) {
		my $h = readline M;
		chomp $h;
		my @H = split(/\t/, $h);
#		print $_ . ":" . $H[$_] . "\n" foreach (0 .. $#H);
		last;		
	}
}
while (<M>) {
	chomp;
	s/\s+$//;
	my @row = split(/\t/, $_);
	push @{$targets{$row[0]}{$_}}, $row[8] foreach ($row[3] .. $row[4]);
	$manif{$row[8]} = [@row];
	if (not exists($stats{$row[8]}{dat})) {
		$stats{$row[8]}{dat} = [@row];
		$stats{$row[8]}{ln} = $row[4] - $row[3] + 1;
		$stats{$row[8]}{n} = 0;
		$stats{$row[8]}{covs} = [];
	}
}
close M;

open C, "<$covtable";
while (<C>) {
	chomp;
	my @row = split(/\t/, $_);
	$#row = 13;
	if (exists($targets{$row[0]}{$row[1]})) {
		foreach my $t (@{$targets{$row[0]}{$row[1]}}) {
			$stats{$t}{n}++; 
			$stats{$t}{covSum} += $row[2]; 
			push @{$stats{$t}{covs}}, $row[2]; 
		}
	}
	print join("\t", @row[0 .. 1], 
		(join("|", @{$targets{$row[0]}{$row[1]} || ["."]} )), 
		(@row[2 .. 11]),
		(map { $_ || "." } @row[12 .. $#row]),
		). "\n";
#	sleep 1;
}
close C;

# Calc assay stats
open QC, ">$QCrep";
foreach my $t (sort keys %stats) {
	my @covs = sort {$a <=> $b} @{$stats{$t}{covs}};
	my $mean = ($stats{$t}{n} != 0)?$stats{$t}{covSum} / $stats{$t}{n}:0;
	#print join("\t", @{$stats{$t}{dat}}, $mean, map { $stats{$t}{$_} } qw/ln n/). "\n";
	print QC join("\t", $t, sprintf("%.2f", $mean), map { $stats{$t}{$_} } qw/ln n/). "\n";
	
}
close QC;
