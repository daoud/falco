#!/usr/bin/perl

use strict;

my $file = shift;
my $qc = shift;
my %crds = (); 
open F, "<$file";
my $fhead = readline F;
chomp $fhead;
print $fhead . "\t" . join("\t", qw/dp dpref dpvar vaf/) . "\n";
while (<F>) {
	chomp;
	my @row = split(/\t/, $_, -1);
#	print STDERR scalar(@row) . "\n";
	push @{$crds{$row[0]}{$row[1]}}, [@row];
}
close F;

# INDELS?
my %ntcol = (A => 8, C => 9, G => 10, T => 11);
open QC, "<$qc";
while (<QC>) {
	chomp;
	my @row = split(/\t/, $_);
	next if ($row[2] eq ".");
	my $muts = $crds{$row[0]}{$row[1]} || undef;
	next unless $muts;
	foreach my $mut (@$muts) {
		my $rl = length($mut->[3]);
		my $vl = length($mut->[4]);

		my $refDP = $row[$ntcol{uc($row[4])}];
	
		if ($rl > $vl && $row[14] ne ".") { # Deletion
			my %dels = split(/[\|\:]/, $row[14]);
			my @del = sort { $dels{$b} <=> $dels{$a} } keys(%dels); 	
			my $deldp = $dels{$del[0]}; 
			print join("\t", @{$mut}, $row[3], $row[6] - $deldp, $deldp, $deldp / $row[3]) . "\n";

		}
		elsif ($rl < $vl && $row[13] ne ".") { # Insertion
			my %inss = split(/[\|:]/, $row[13]);
			my @ins = sort { $inss{$b} <=> $inss{$a} } keys(%inss); 	
			my $insdp = $inss{$ins[0]};
			print join("\t", @{$mut}, $row[3], $row[6] - $insdp, $insdp, $insdp / $row[3]) . "\n";
		}
		elsif ($rl == $vl) {
			my @varnts = sort { $row[$ntcol{$b}] <=> $row[$ntcol{$a}]  } split(/\//, $row[5]);
			next if ($row[$ntcol{$varnts[0]}] == $row[$ntcol{$varnts[1]}]); # Equeal variant counts
			my $vardp = $row[$ntcol{$varnts[0]}];
			print join("\t", @{$mut}, $row[3], $row[6], $vardp, $vardp / ($row[6] + $vardp)) . "\n";
		}

	}
	next;
#	if (exists($crds{$row[0]}{$row[1]}) && length($crds{$row[0]}{$row[1]}->[3]) > 1) { # In case of snp
	if (exists($crds{$row[0]}{$row[1]})) { # && length($crds{$row[0]}{$row[1]}->[3]) > 1) { # In case of snp
		print join("\t", @{$_}, $row[3], $row[6], $row[7], $row[7] / $row[3]) . "\n" foreach @{$crds{$row[0]}{$row[1]}};
	}
	if ($row[14] ne "." && exists($crds{$row[0]}{$row[1]})) { # In case of deletion!
		my %dels = split(/[\|\:]/, $row[14]);
		my @del = sort { $dels{$b} <=> $dels{$a} } keys(%dels); 	
		$row[7] = $dels{$del[0]}; 
		print join("\t", @{$_}, $row[3], $row[6] - $row[7], $row[7], $row[7] / $row[3]) . "\n" foreach @{$crds{$row[0]}{$row[1]}};
	}
	if ($row[13] ne "." && exists($crds{$row[0]}{$row[1]})) { # In case of insertion
		my %inss = split(/[\|:]/, $row[13]);
		my @ins = sort { $inss{$b} <=> $inss{$a} } keys(%inss); 	
		$row[7] = $inss{$ins[0]};
		print join("\t", @{$_}, $row[3], $row[6] - $row[7], $row[7], $row[7] / $row[3]) . "\n" foreach @{$crds{$row[0]}{$row[1]}};
	}
}
close QC;
