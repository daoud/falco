#!/usr/bin/perl -w 
#

use strict;

my $ref = shift;
my $targets = shift;
my $fai = $ref . ".fai";
my %idx = ();
$| = 1;
open FAI, "<$fai";

print STDERR "Reading fa index ... ";
while (<FAI>) {
	chomp;
	my @row = split(/\t/, $_); 
	$idx{$row[0]} = [@row];	
}
print STDERR "done\n";

close FAI;

# Read in targets and only process alignemts if start or end is within 5bp of the primer start.
my %primes = ();
open T, "<$targets";
while (<T>) {
	my @row = split(/\t/, $_);
	#456
	$primes{"$row[4]" . $_} = $row[0] foreach (($row[5] - 5) .. ($row[5] + 5), ($row[6] - 5) .. ($row[6] + 5));
}
close T;

open REF, "<$ref";

my $ppos = 0;
my $pchr = "";
my %data = ();
my $refSeq = "";
my $c = 0;
my $col_chr = 2;
my $col_read = 9;
my $col_qual = 10;
my $col_pos = 3;
my $col_cigar = 5;
my $refSeqPad = "P";

my $cnt = 1;
while (<STDIN>) {

	# Skip stampy reads?
	next unless (/XP:Z:BWA/);

	my @row = split(/\t/, $_);
#	print STDERR $row[$col_pos] . "\n";
#	my $aln = "";
	if (($pchr ne $row[$col_chr])) { # flush data when chr changes or when target changes?
		&flush(\%data, $pchr);
		$pchr = $row[$col_chr];
		last if ($pchr eq "*");
		print STDERR "$pchr - Processing ... ";		
	}
	my $read = $row[$col_read];
	my $qual = $row[$col_qual];
	my $pos = $row[$col_pos];
	my @cig = ();
	my $aln = 0;
	while ($row[$col_cigar] =~ /(\d+)(\D)/g) {
		push @cig, [$1, $2];
		$aln += $1;
	}
	if ($ppos != $pos) {
		my $offset = $idx{$row[$col_chr]}->[2];
		my $nl = int (($row[$col_pos] - 1) / $idx{$row[$col_chr]}->[3]);
		$offset += $row[$col_pos] + $nl - 1;
		seek REF, $offset, 0;
		$refSeqPad = substr($refSeq, -1, 1);
		read REF, $refSeq, 1000; # length($read) * 5;
		$refSeq =~ s/\s//g;
#		$data{$row[$col_chr]}{$row[$col_pos] + $_}{"refNT"} = substr($refSeq, $_, 1) foreach (0 .. (length($refSeq) - 1));
		$data{$row[$col_chr]}{$row[$col_pos] + $_}{"refNT"} = substr($refSeq, $_, 1) foreach (0 .. $aln);
		$ppos = $pos;
	}
#	while ($row[$col_cigar] =~ /(\d+)(\D)/g) {
	foreach my $cigE (@cig) {
		my $n = $cigE->[0];	
		if ($cigE->[1] eq "I") {
#			my $insert = substr($read, 0, $1, "");
#			$data{$row[$col_chr]}{$pos}{"I"}{$insert}++;
			my $rpos = $pos - $row[$col_pos];
			my $insert = substr($refSeq, $rpos - 1, 1) or print STDERR "Outside?" . $rpos . ":" . length($refSeq) . " $pos\n";
			$insert .= substr($read, 0, $n, "");
			my $qinsert .= substr($qual, 0, $n, "");
			$data{$row[$col_chr]}{$pos - 1}{"I"}{$insert}++;
#			print STDERR "+";
		}
		elsif ($cigE->[1] eq "M") {
			my $mseq = substr($read, 0, $n, "");
			my $mqual = substr($qual, 0, $n, "");
			foreach my $nt (0 .. (length($mseq) - 1)) {
				$data{$row[$col_chr]}{$pos + $nt}{"DP"}++;
				my $ntq = substr($mqual, $nt, 1);
				# TODO Changed from scalar to an array
#				push @{$data{$row[$col_chr]}{$pos + $nt}{"NT"}{substr($mseq, $nt, 1)}}, $ntq;
				$data{$row[$col_chr]}{$pos + $nt}{"NT"}{substr($mseq, $nt, 1)}++;
#				print STDERR join(":", $row[$col_chr], $pos, $nt, substr($mseq, $nt, 1)) . "\n"; sleep 1;
			}
			$pos += $n;		
#			print STDERR "M";
			
		}
		elsif ($cigE->[1] eq "D") {
#			my $rpos = $pos - $row[$col_pos];
#			my $deletion = substr($refSeq, $rpos, $1); # Substring out of string error ?
#			$data{$row[$col_chr]}{$pos}{"D"}{$deletion}++;
#			$pos += $1;
			my $rpos = $pos - $row[$col_pos];
			my $deletion = substr($refSeq, $rpos - 1, $n + 1); # Substring out of string error ?
			$data{$row[$col_chr]}{$pos - 1}{"D"}{$deletion}++;
			$pos += $n;
#			print STDERR "-";
		}
	}
}
&flush(\%data, $pchr);

close REF;

sub match {
	my $ref = shift;
	my $seq = shift;
	my $k = 5;
	my $p = 0;
	my @mm = ();
	while (my $r = substr($$ref, $p, $k)) {
		my $s = substr($$seq, $p, $k);
		if ($r ne $s) {
#			my $aln = "";
#			print "Mismatch!\n";
			foreach my $i (0 .. $k - 1) {
				if (substr($r, $i, 1) ne substr($s, $i, 1)) {
#					$aln .= "x";
					push @mm, [$p + $i, substr($r, $i, 1), substr($s, $i, 1)];
				}
				else {
#					$aln .= "|";
				}
			}
#			print $r . "\n$aln\n" . $s . "\n";		
			
		}
		else {
#			print $r . "\n" . $s . "\n";		
		}
		$p += $k;
	}

	return \@mm;	
}

sub flush {
	my $data = shift;
	my $chr = shift;
	return unless (exists($data->{$chr}));
	print STDERR "printing ... ";
	foreach my $p (sort {$a <=> $b} keys(%{$data->{$chr}})) {
		my $posdat = $data->{$chr}{$p} || next;
		next unless (exists($posdat->{DP}));
		my $refNT = uc($posdat->{refNT});
		my @varOrd = sort { $posdat->{NT}{$b} <=> $posdat->{NT}{$a} } grep { $_ ne $refNT  } keys(%{$posdat->{NT}});
		my $seqNT = join("/", @varOrd);
		#my $refCNT = scalar(@{$posdat->{NT}{$refNT} || []});
		my $refCNT = $posdat->{NT}{$refNT} || 0;
		my $varCNT = 0;
		#$varCNT += scalar(@{$posdat->{NT}{$_}}) foreach (@varOrd);
		$varCNT += $posdat->{NT}{$_} foreach (@varOrd);
		my $Istr = ".";
		my $Dstr = ".";
		$Istr = join("|", map { $_ . ":" . $posdat->{I}{$_} } keys(%{$posdat->{I}})) if exists($posdat->{I});
		$Dstr = join("|", map { $_ . ":" . $posdat->{D}{$_} } keys(%{$posdat->{D}})) if exists($posdat->{D});
		print join("\t", $chr, $p,
				$posdat->{DP} || 0,
				$posdat->{refNT} || ".",
				$seqNT || ".", 
				$refCNT, $varCNT,
				#(map { scalar(@{$posdat->{NT}{$_} || []})} qw(A C G T N)), $Istr, $Dstr,
				(map { $posdat->{NT}{$_} || 0 } qw(A C G T N)), $Istr, $Dstr,
			  ) . "\n";
#		delete $data->{$chr}{$p};
	}
	delete $data->{$chr};
	print STDERR "done\n";
}
