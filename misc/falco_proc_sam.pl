#!/usr/bin/perl -w

use strict;
use Time::HiRes qw(gettimeofday tv_interval);

my $bam = shift;
my $ref = shift || "/gsdata/tmp/TSACP/ampliconPipeLine/ref/hg19/bwa-5/hg19.fa";
my $bedManifest = shift;
my $base = shift || "output";
my $samtools = shift || "/gsdata/tmp/TSACP/ampliconPipeLine/bin/samtools/samtools-0.1.18/samtools";
my $fai = $ref . ".fai";
my $Qthreshold = 10;
my $QavgLim = 20;

open FAI, "<$fai";
my %idx = ();
print STDERR "Reading fa index ... ";
while (<FAI>) {
        chomp;
        my @row = split(/\t/, $_);
        $idx{$row[0]} = [@row];
}
print STDERR "done\n";

close FAI;
open REF, "<$ref";

my %stats = ();
my %targets = ();
my %hash = ();

open B, "<$bedManifest";
while (<B>) {
	chomp;
	my @row = split(/\t/, $_);
	my $length = $row[2] - $row[1];
	my $ampliconName = $row[3];
	if (not exists($stats{$ampliconName}{target})) {
		$stats{$ampliconName}{target} = [@row];
		$stats{$ampliconName}{ln} = $length;
		$stats{$ampliconName}{n} = 0;
                $stats{$ampliconName}{sam} = [];
                $stats{$ampliconName}{depth} = 0;
                $stats{$ampliconName}{covs} = [];
                $stats{$ampliconName}{chr} = $row[0];
                $stats{$ampliconName}{start} = $row[1];
                $stats{$ampliconName}{end} = $row[2];
                $stats{$ampliconName}{assayStart} = $row[1];# + length($probe{$row[0]}{ULSO});
                $stats{$ampliconName}{assayEnd} = $row[2];# - length($probe{$row[0]}{DLSO});
	}
	foreach my $i (0 .. 5) {
		$hash{$row[0]}{$row[1] + $i} = [$ampliconName, $length];
		$hash{$row[0]}{$row[1] - $i} = [$ampliconName, $length];
	}
}

# Open output 
open QC, ">$base.qc.ann.txt";
open QC2, ">$base.qc2.ann.txt";
open TARGET, ">$base.qc.targets.txt";

open SAM, "$samtools view $bam |";
#open SAM, "$samtools view -b $bam | $samtools calmd - $ref |";
#open SAM, "$samtools view -b $bam $coord  | $samtools calmd - $ref |";

while (<SAM>) {
	last unless (/^@/);
}
my $cnt = 0;
my %hsh = ();
my $pamp = ".";

print QC "#" . join("\t", qw/chr pos amp dp ntRef ntVar nRef nVar nA nC nG nT nN ins del/) . "\n";
print QC2 "#" . join("\t", qw/read chr pos amp event/) . "\n";
print TARGET "#" . join("\t", qw/amp chr start end assayStart assayEnd depth/) . "\n";
while (<SAM>) {
	chomp;
	my @row = split(/\t/, $_);
	my $l = 0;
	my $cig = $row[5];
	$cig =~ s/(\d+)[MD]/$l+=$1/eg;

	my $rec = $hash{$row[2]}{$row[3]} || next;
	my $amp = $rec->[0];
	my $ampLength = $rec->[1];
	
	next if (abs($l - $ampLength) >= 5);

	push @{$stats{$amp}{sam}}, [@row];
	$stats{$amp}{depth}++;

	if (($pamp ne ".") && ($amp ne $pamp)) {
		print STDERR "Flushing $pamp $stats{$pamp}{depth}\n";
		my $data = &call($stats{$pamp}{sam});
		&flush($data, $stats{$pamp}{chr}, $pamp, $stats{$pamp}{assayStart}, $stats{$pamp}{assayEnd});
		$stats{$pamp}{sam} = [];
	}
	$pamp = $amp;
	$cnt++;
}

close QC;
close QC2;
foreach my $pamp (keys %stats) {
		print TARGET join("\t", $pamp, map { $stats{$pamp}{$_} || "NA"} qw/chr start end assayStart assayEnd depth/) . "\n";
}
close TARGET;
	

close SAM;

sub call {
	my $SAM = shift;
	my $ppos = 0;
	my $pchr = "";
	my %data = ();
	my $refSeq = "";
	my $c = 0;
	my $col_chr = 2;
	my $col_read = 9;
	my $col_qual = 10;
	my $col_pos = 3;
	my $col_cigar = 5;
	my $refSeqPad = "P";
	my %cooc = ();
	my $cnt = 1;

	foreach my $r (0 .. $#$SAM) {
		my @row = @{$SAM->[$r]};
		my $read = $row[$col_read];
		my $qual = $row[$col_qual];
		my $pos = $row[$col_pos];
		my @cig = ();
		my $aln = 0;

		# Calculate mean read quality and skip if it's below threshold
		my $QSum = 0;
		my $Qlen = length($qual);

		$QSum += ord($qual) foreach 1 .. $Qlen;
		
		my $Qavg = $QSum / $Qlen - 33;
		next if ($Qavg < $QavgLim);

		while ($row[$col_cigar] =~ /(\d+)(\D)/g) {
			push @cig, [$1, $2];
			$aln += $1;
		}
		if ($ppos != $pos) {
			my $offset = $idx{$row[$col_chr]}->[2];
			my $nl = int (($row[$col_pos] - 1) / $idx{$row[$col_chr]}->[3]);
			$offset += $row[$col_pos] + $nl - 1;
			seek REF, $offset, 0;
			$refSeqPad = substr($refSeq, -1, 1);
			read REF, $refSeq, 1000;#  length($read) * 5;
			$refSeq =~ s/\s//g;
			$data{$row[$col_chr]}{$row[$col_pos] + $_}{"refNT"} = substr($refSeq, $_, 1) foreach (0 .. $aln);
			$ppos = $pos;
		}

		foreach my $cigE (@cig) {
			my $n = $cigE->[0];
			if ($cigE->[1] eq "I") {
				my $rpos = $pos - $row[$col_pos];
				my $insert = substr($refSeq, $rpos - 1, 1) or print STDERR "Outside?" . $rpos . ":" . length($refSeq) . " $pos\n";
				$insert .= substr($read, 0, $n, "");
				#my $qinsert .= substr($qual, 0, $n, "");
				#push @{$data{$row[$col_chr]}{$pos - 1}{"IR"}{$insert}}, $r;
				#$data{$row[$col_chr]}{$pos - 1}{"I"}{$insert}++;
				push @{$data{$row[$col_chr]}{$pos - 1}{"I"}{$insert}}, $r;
			}
			elsif ($cigE->[1] eq "M") {
				my $mseq = substr($read, 0, $n, "");
				
				my $mqual = substr($qual, 0, $n, "");
				foreach (0 .. (length($mseq) - 1)) {
					my $ntn = substr($mseq, $_, 1);
				#	my $ntq = substr($mqual, $_, 1);
				#	if (ord($ntq) - 33  < $Qthreshold) {
				#		$ntn = "N";
				#	}
				#	$data{$row[$col_chr]}{$pos + $nt}{"NT"}{$ntn}++;
					push @{$data{$row[$col_chr]}{$pos + $_}{"NT"}{$ntn}}, $r;
				}
				$pos += $n;

			}
			elsif ($cigE->[1] eq "D") {
				my $rpos = $pos - $row[$col_pos];
				my $deletion = substr($refSeq, $rpos - 1, $n + 1); # Substring out of string error ?
				#push @{$data{$row[$col_chr]}{$pos - 1}{"DR"}{$deletion}}, $r;
				push @{$data{$row[$col_chr]}{$pos - 1}{"D"}{$deletion}}, $r;
				$pos += $n;
			}
		}
		$cnt++;
	}
	return \%data;
}

sub flush {
        my $data = shift;
        my $chr = shift;
	my $amp = shift;
	my $s = shift;
	my $e = shift;
        return unless (exists($data->{$chr}));
        print STDERR "printing ... ";
	my %reads = (); # read->pos->nt
#        foreach my $p (sort {$a <=> $b} keys(%{$data->{$chr}})) {
#        	unless (($p >=  $s) && ($p <= $e)) {
#			next;	
#		}
	foreach my $p ($s .. $e) {
	        my $posdat = $data->{$chr}{$p} || next;
	        next unless (exists($posdat->{NT}));
                my $refNT = uc($posdat->{refNT});
                my @varOrd = sort { scalar(@{$posdat->{NT}{$b}}) <=> scalar(@{$posdat->{NT}{$a}}) } grep {$_ ne "N"} grep { $_ ne $refNT  } keys(%{$posdat->{NT}});
                my $seqNT = join("/", @varOrd);
                #my $refCNT = scalar(@{$posdat->{NT}{$refNT} || []});
		my $refCNT = scalar(@{$posdat->{NT}{$refNT} || []});
		my $varCNT = 0;
		#$varCNT += scalar(@{$posdat->{NT}{$_}}) foreach (@varOrd);
		#$varCNT += $posdat->{NT}{$_} foreach (@varOrd);
		$varCNT = ($varOrd[0])?scalar(@{$posdat->{NT}{$varOrd[0]}}):0;
		my $DP = $refCNT;
		$DP += scalar(@{$posdat->{NT}{$_}}) foreach @varOrd;
		
		#print "Aaargh\n" if ($posdat->{DP} != $DP);

		my $Istr = ".";
		my $Dstr = ".";
		if ($varOrd[0]) {
			#$reads{$_}{$p} = "$refNT/$varOrd[0]" foreach (@{$posdat->{NTR}{$varOrd[0]}});
			$reads{$_}{$p} = "$refNT/$varOrd[0]" foreach (@{$posdat->{NT}{$varOrd[0]}});
		}
		if (exists($posdat->{I})) {
			$Istr = join("|", map { $_ . ":" . scalar(@{$posdat->{I}{$_}}) } keys(%{$posdat->{I}}));
			foreach my $I (keys(%{$posdat->{I}})) {
				$reads{$_}{$p} = "$refNT/$I" foreach (@{$posdat->{I}{$I}});
			}
		}
		if (exists($posdat->{D})) {
			$Dstr = join("|", map { $_ . ":" . scalar(@{$posdat->{D}{$_}}) } keys(%{$posdat->{D}}));
			foreach my $D (keys(%{$posdat->{D}})) {
				$reads{$_}{$p} = "$D/$refNT" foreach (@{$posdat->{D}{$D}});
			}
		}
		#$Dstr = join("|", map { $_ . ":" . $posdat->{D}{$_} } keys(%{$posdat->{D}})) if exists($posdat->{D});
		print QC join("\t", $chr, $p, $amp,
				$DP || 0,
				$posdat->{refNT} || ".",
				$seqNT || ".",
				$refCNT, $varCNT,
				#(map { scalar(@{$posdat->{NT}{$_} || []})} qw(A C G T N)), $Istr, $Dstr,
				(map { scalar(@{$posdat->{NT}{$_} || []})} qw(A C G T N)), $Istr, $Dstr,
			  ) . "\n";
		# delete $data->{$chr}{$p};
	}
	delete $data->{$chr};
	foreach my $r (sort { $a <=> $b } keys(%reads)) {
		print QC2 join("\t", $r, $chr, $_, $amp, $reads{$r}{$_}) . "\n" foreach (sort {$a <=> $b} keys(%{$reads{$r}})); 
	}
	print STDERR "done\n";
}

