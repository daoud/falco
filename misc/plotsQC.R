#!/usr/bin/Rscript 

args <- commandArgs(T)
print(args[1])
print(args[2])

read.delim(args[1], header=F, stringsAsFactors=F) -> alnStats
read.delim(args[2], header=F, stringsAsFactors=F) -> errStats

library(RColorBrewer)
#cols <- brewer.pal(3, "Blues")
cols <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

xlegend <- 1.2 * nrow(alnStats) * 1.15
png("alnStats.png", width=960, height=480, res=72)

alnStats[,2:5] / 1000000 -> alnStats[,2:5]
fair <- mean(alnStats[,2])
sub(".bam$", "", alnStats[,1]) -> alnStats[,1]
sub(".qc.ann.txt$", "", errStats[,1]) -> errStats[,1]

oldMar <- par()$mar
par(mar=c(8.1, 4.1, 4.1, 6.1), xpd=T)
barplot(as.matrix(t(alnStats[order(alnStats[,1]),c(3:5)])), names.arg=alnStats[order(alnStats[,1]),1], las=3, col=cols[1:3], main="Alignment efficiency", ylab="Reads (Millions)", legend.text=c("Ontarget", "Offtarget", "Unmapped"), angle=.5, args.legend=c(x=xlegend))
par(xpd=F)
abline(h=fair, lty=2)
dev.off()

png("errStats.png", width=960, height=480, res=72)
par(mar=c(8.1, 4.1, 4.1, 6.1))
barplot(errStats$V4[order(errStats[,1])], names.arg=errStats[order(errStats[,1]),1], las=3, col=cols[1], main="Error rate", ylab="%")

abline(h=1, lty=2)
dev.off()

png("qualStats.png", width=960, height=480, res=72)
par(mar=c(8.1, 4.1, 4.1, 6.1), xpd=T)
barplot(as.matrix(t(alnStats[order(alnStats[,1]),c(6:9)])), names.arg=alnStats[order(alnStats[,1]),1], las=3, col=cols[1:4], main="Quality stats", ylab="%", legend.text=c("Q > 30", "30 < Q >= 20", "20 < Q >= 10", "Q < 10"), angle=.5, args.legend=c(x=xlegend))
dev.off()


#library(ggplot2)
#png("alnStatsgg.png", width=960, height=480, res=72)
#df <- data.frame(
#	sample= c(rep(alnStats[,1], 3)),
#	target = c(rep("ontarget", nrow(alnStats)), rep("offtarget", nrow(alnStats)), rep("unmapped", nrow(alnStats))),
#	count = c(alnStats[,3], alnStats[,4], alnStats[,5])
#)
#
#ggplot(data=df, aes(x=sample, y=count, fill=target)) + geom_bar(stat="identity")
#dev.off()
