#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

base=$1

vcf2tsv=$DIR/../../misc/vcf2tsv.pl
spliteff=$DIR/splitEff.pl
addQc=$DIR/addQc.pl
filter=$DIR/filter.pl

# convert to tsv
$vcf2tsv $base.samtools.vcf > $base.samtools.tsv
$vcf2tsv $base.vs.snp.a.vcf > $base.vs.snp.a.tsv

# split eff columns
$spliteff $base.samtools.tsv > $base.res.tmp
$spliteff $base.vs.snp.a.tsv >> $base.res.tmp

# add count data
$addQc $base.res.tmp $base.qc.ann.txt > $base.res.tsv

# Filter
$filter $base.res.tsv > $base.res.filtered.tsv
