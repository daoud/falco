#!/usr/bin/perl -w

use strict;
use Sys::Hostname;

my $dir = shift || undef;
my $tMax = shift || 4;
my @files = ();
my %pairs = ();
#if ($#ARGV == -1) {

unless (-d $dir) {
	&stderr($dir . " not found");
	exit;
}

if ($dir) {
	chdir $dir;
	open FILES, "find -L ./ -iname \"*fastq.gz\"|";
	while (<FILES>) {
		chomp;
		push @files, $_;
	}
}
else {
	while (<STDIN>) {
		chomp;
		push @files, $_;
	}
}

if ($#files == -1) {
	&stderr("No fastq found, exiting");
	exit;
}

foreach my $f (@files) {
	chomp $f;
	$f =~ /^(.*)\/(.*)_(.*?)_(.*?)_(.*?)_(.*?)$/;
	#next if ($3 =~ /Undetermined/);
	#print $2 . " $f\n";
	$pairs{$2}{$5} = $f;

}
#my $binDir = "/gsdata/tmp/TSACP/ampliconPipeLine";
my $binDir = "/gsdata/Analysis/falco";
my $bin = "$binDir/amplicon-Al-TSACP.sh";
my $dstamp = `date +"%y%m%d"`;
my $tCnt = 0;
my %pidHsh = ();
#print STDERR "PID $$\n";
&stderr("PID $$");
foreach my $pair (keys(%pairs)) {
	#next if ($pair =~ /Undetermined/);
	my $pid = fork;
	print "$$ $pid \n";
	sleep 1;
	if ($pid) {
		$pidHsh{$pid} = localtime();
#		print STDERR localtime() . " : Forking $pid : $pair\n";
		&stderr("Forking $pid : $pair");
		while (scalar(keys(%pidHsh)) >= $tMax) {
			my $deceased = wait;
			#print STDERR localtime() . " : $deceased $pidHsh{$deceased} finished\n" if (exists($pidHsh{$deceased}));
			&stderr("$deceased $pidHsh{$deceased} finished") if (exists($pidHsh{$deceased}));
			delete($pidHsh{$deceased});
		}
		sleep 1;			
	}
	else {
#		print STDERR localtime() . " : Child $$ forked??\n";
		&stderr("Child $$ forked");
		system("$bin $pairs{$pair}{R1} $pairs{$pair}{R2} $pair > $pair.log 2>&1");
		&stderr("Child $$ done");
		exit;
		#print STDERR localtime() . " : Child $$ I should be dead by now,...\n";
	}
}

1 while (wait() != -1);

my $bin2 = "$binDir/amplicon-QC.sh";
#print STDERR localtime() . " : Generating run QC $bin2\n";
&stderr("Generating run QC $bin2");
system($bin2);
my $bin3 = "$binDir/misc/mkHtmlReport.pl ./ ./";
#print STDERR localtime() . " : Generating HTML $bin3\n";
&stderr("Generating HTML $bin3");
system($bin3);

sub stderr {
	my $msg = shift;
	print STDERR localtime() . " [" . hostname . "] [$$] : $msg\n"; 
}
