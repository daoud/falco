#!/usr/bin/perl -w
#

use strict;

my $file = shift;
my $canonicals = shift;

my %can = ();
my %nm = ();

open CAN, "<$canonicals";

while (<CAN>) {
	chomp;
	my @row = split(/\t/, $_);
	$can{$row[0]} = @row[1 .. $#row];
	my $pri = 0;
	$nm{$_} = $pri++ foreach @row[1 .. $#row];
#	print STDERR join(":", @row[0,1]) . "\n"; sleep 1;
}

open F, "<$file";
my $head = readline(F);
chomp $head;
print $head . "\tvaf\n";
my @rowHead = split(/\t/, $head);
my $colN = 0;
my %col = map { 
#	print STDERR $_ . " $colN\n"; 
	$_ => $colN++ } @rowHead;

while (<F>) {
	chomp;
	my $line = $_;
	my @row = split(/\t/, $_);
	#g12 t15
	next unless (exists($can{$row[$col{Gene_Name}]}));
	my $trans = $row[$col{Transcript_ID}];
	$trans =~ s/\..*$//;
	
	if (not exists($nm{$trans})) {
		print STDERR "Unknown transcript: $row[$col{Gene_Name}] $trans\n";
		next;
	}
	elsif ($nm{$trans} != 0) {
		print STDERR "Non cannonical: $row[$col{Gene_Name}] $trans\n";
		next;
	}

	#next unless ($nm{$trans} == 0);
			
	# Do not filter out clinicvar loci
	#print $_ . "\n" if ($row[$col{Tag}] eq "Clinvar");
	next if ($row[$col{Tag}] eq "Clinvar");
#	print STDERR $row[15]; sleep 1;
#	print STDERR $nm{$row[15]}; sleep 1;

#CODON_CHANGE_PLUS_CODON_DELETION
#CODON_DELETION
#DOWNSTREAM
#EXON
#FRAME_SHIFT
#INTRON
#NON_SYNONYMOUS_CODING
#SPLICE_SITE_ACCEPTOR
#SYNONYMOUS_CODING
#UPSTREAM
#UTR_3_PRIME
	
	# Filter INTERGENIC
	next if ($row[$col{Context}] eq "INTERGENIC");
	
	# Filter SYNONYMOUS_CODING 
	# Dubbel mutatie BRAF 
#	next if ($row[$col{Context}] eq "SYNONYMOUS_CODING");
	
	# Filter INTRON
	next if ($row[$col{Context}] eq "INTRON");

	# Filter dbSNP 
#	next if ($row[2] ne ".");

	# Filter depth
	next if ($row[$col{DP}] < 100);
	
	my ($nref, $nvar) = split(/,/, $row[$col{AD}]);	
	# Filter var depth
#	print STDERR "DEBUG: " . $nvar . "\n"; sleep 1;
	next if ($nvar < 10);
	
	my $vaf = $nvar / ($nref + $nvar);
#	print STDERR "DEBUG: " . $vaf . "\n";
	# Filter vaf
	next if ($vaf < 0.01);
	

	print $_ . "\t$vaf\n";	
}
close F;
