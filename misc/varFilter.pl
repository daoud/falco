#!/usr/bin/perl
#
# Take target regions and call variants based on simple depth en frequency thresholds

my $minDepth = shift || 100;
my $minFreq = shift || 0.01;
my $minVar = shift || 10;

my $depthCol = 3;
my $varCol = 7;
my %nt = (
	A => 8,
	C => 9,
	G => 10,
	T => 11,
);
my $input = shift;
my $fh = undef;
if (defined $input) {
	open ($fh,  "<$input");
}
else {
	$fh = *STDIN unless (defined $fh);
}

#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  Mutant25
my $prevNT = "";
print "##fileformat=VCFv4.1\n";
print "##INFO=<ID=TARGET,Number=1,Type=String,Description=\"Target description\">\n";
print "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Raw read depth\">\n";
print "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Raw read depth\">\n";
print "##FORMAT=<ID=AD,Number=2,Type=Integer,Description=\"Allele Distribution\">\n";
print "##FORMAT=<ID=VAF,Number=1,Type=Float,Description=\"Variant Allele Frequency\">\n";
print "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tsample\n";

while (<$fh>) {
	chomp;
	my @row = split(/\t/, $_);
	
	my %var = map { $_ , $row[$nt{$_}]  } split(/\//, $row[5]);
	my $varNT = (sort { $var{$b} <=> $var{$a} } keys(%var))[0];
	
	my $depth = $row[$depthCol];
	my $freq = $var{$varNT} / $depth;
	if (($row[2] ne ".") && ($minDepth <= $depth)) {
		my $qual = 100;
		my $filt = ".";
		$row[2] =~ s/\|/,/g;
		my $info = "TARGET:$row[2];DP:$depth";

		if ($row[13] ne ".") {
			my %ins = split(/[\|:]/, $row[13]);
			my $maxIns = (sort {$ins{$b} <=> $ins{$a}} keys(%ins))[0];
			my $iFreq = $ins{$maxIns};
			my $VAF = $iFreq / ($iFreq + $row[6]);
			$maxIns =~ s/^\+//;
			if ($VAF > $minFreq && $minVar <= $iFreq) {
				my $format = "DP:AD:VAF\tDP=$depth:AD=$row[6],$iFreq:VAF=$VAF";
				print join("\t", @row[0 .. 1], ".", $prevNT . $row[4], $prevNT . $row[4] . $maxIns, $qual, $filt, $info, $format) . "\n";
			}
		}

		if ($row[14] ne ".") {
			my %del = split(/[\|:]/, $row[14]);
			my $maxDel = (sort {$del{$b} <=> $del{$a}} keys(%del))[0];
			my $dFreq = $del{$maxDel};
			my $VAF = $dFreq / ($dFreq + $row[6]);
			$maxDel =~ s/^-//;
			if ($VAF > $minFreq && $minVar <= $dFreq) {
				my $format = "DP:AD:VAF\tDP=$depth:AD=$row[6],$dFreq:VAF=$VAF";
				print join("\t", @row[0 .. 1], ".", $prevNT . $maxDel, $prevNT, $qual, $filt, $info, $format) . "\n";
			}
		}


		if ($minFreq <= $freq && $minVar <= $var{$varNT}) {
			my $format = "DP:AD:VAF\tDP=$depth:AD=$row[6],$var{$varNT}:VAF=$freq";
			my $varNT = substr($row[5], 0, 1);
			print join("\t", @row[0 .. 1], ".", $row[4], $varNT, $qual, $filt, $info, $format) . "\n";
		}
	}
	$prevNT = $row[4];	
}
