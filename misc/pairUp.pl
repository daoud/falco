#!/usr/bin/perl -w

use strict;

my $r1 = shift;
my $r2 = shift;
my $o1 = shift || "$r1.paired";
my $o2 = shift || "$r2.paired";
my $min = 10;
my %hsh = ();

open R1, "<$r1";
while (<R1>) {
	/(.*?)\s/;
	my $k = $1;
	my $h = $_; 
	my $seq = readline(R1);
	my $qual = readline(R1); 
	$qual .= readline(R1); 
	if (length($seq) > $min) {
		$hsh{$k} = $h . $seq . $qual;
	}
	else {
#		print STDERR "$h$seq too short\n"; sleep 1;
	}
}
close R1;

open R2, "<$r2";
open O1, ">$o1";
open O2, ">$o2";

while (<R2>) {
	/(.*?)\s/;
	my $k = $1;
	my $h = $_; 
	my $seq = readline(R2);
	my $qual = readline(R2); 
	$qual .= readline(R2); 
	if (length($seq) > $min) {
	}
	else {
#		print STDERR "$h$seq too short\n"; sleep 1;
		next;
	}
	if (exists($hsh{$k})) {
		print O1 $hsh{$k};
		print O2 $h . $seq . $qual;
	}
	else {
#		print STDERR "$k is missing\n"; sleep 1;
	}
}
close R2;
close O1;
close O2;
