#!/usr/bin/perl -w
#
# TODO: tiled targets give wrong length...
use strict;

my $manifest = shift;
my $covtable = shift;

my $QCrep = shift || "qc.txt"; 

my %targets = ();
my %stats = ();
my %manif = ();
open M, "<$manifest";
while (<M>) { last if (/\[Probes\]/) }
readline(M);
while (<M>) {
	chomp;
	if (/\[Targets\]/) {
		my $h = readline M;
		chomp $h;
		my @H = split(/\t/, $h);
#		print $_ . ":" . $H[$_] . "\n" foreach (0 .. $#H);
		last;		
	}	
	my @row = split(/\t/, $_);
	$stats{$row[2]}{probe} = [@row];
	$stats{$row[2]}{ULSO} = $row[9]; 
	$stats{$row[2]}{DLSO} = $row[11]; 
	
	
}
while (<M>) {
	chomp;
	s/\s+$//;
	my @row = split(/\t/, $_);
	push @{$targets{$row[3]}{$_}}, $row[0] foreach ($row[4] .. $row[5]);
	$manif{$row[0]} = [@row];
	if (not exists($stats{$row[0]}{target})) {
		$stats{$row[0]}{target} = [@row];
		$stats{$row[0]}{ln} = $row[5] - $row[4] + 1;
		$stats{$row[0]}{n} = 0;
		$stats{$row[0]}{covs} = [];
		$stats{$row[0]}{chr} = $row[3];
		$stats{$row[0]}{start} = $row[4];
		$stats{$row[0]}{end} = $row[5];
		$stats{$row[0]}{assayStart} = $row[4] + length($stats{$row[0]}{ULSO});
		$stats{$row[0]}{assayEnd} = $row[5] - length($stats{$row[0]}{DLSO});
	}
}
close M;

open C, "<$covtable";
while (<C>) {
	chomp;
	my @row = split(/\t/, $_);
	$#row = 13;
	if (exists($targets{$row[0]}{$row[1]})) {
		foreach my $t (@{$targets{$row[0]}{$row[1]}}) {
			$stats{$t}{n}++; 
			$stats{$t}{covSum} += $row[2]; 
			push @{$stats{$t}{covs}}, $row[2]; 
		}
	}
	print join("\t", @row[0 .. 1], 
		(join("|", @{$targets{$row[0]}{$row[1]} || ["."]} )), 
		(@row[2 .. 11]),
		(map { $_ || "." } @row[12 .. $#row]),
		). "\n";
#	sleep 1;
}
close C;

# Calc assay stats
open QC, ">$QCrep";
foreach my $t (sort keys %stats) {
	my @covs = sort {$a <=> $b} @{$stats{$t}{covs} || []};
	my $mean = ($stats{$t}{n} != 0)?$stats{$t}{covSum} / $stats{$t}{n}:0;
	#print join("\t", @{$stats{$t}{dat}}, $mean, map { $stats{$t}{$_} } qw/ln n/). "\n";
	print QC join("\t", $t, sprintf("%.2f", $mean), map { $stats{$t}{$_} } qw/ln n chr start end assayStart assayEnd/). "\n";
	
}
close QC;
