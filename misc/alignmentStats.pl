#!/usr/bin/perl -w

use strict;

my $targetBed = shift;
my @bams = @ARGV;
$|= 1;

foreach my $bam (@bams) {
	my $file = `basename $bam`;
	chomp $file;
	my $res0 = `samtools idxstats $bam`;
	my @Qdat = ();
	my %Q = ();
	my $mapped = 0;
	my $ontarget = 0;
	my $unmapped = 0;
	while ($res0 =~ /(\S+)\s(\S+)\s(\S+)\s(\S+)\n/g) {
		if ($1 ne "*") {
			$mapped += $3;
			$unmapped += $4;
		}
		else {
			$unmapped += $3;
			$unmapped += $4;
		}
	}
	my %Qcon = map { chr($_ + 33) => $_  } (0 .. 40);	
	open PIPE, "samtools view -XF0x014 -L $targetBed $bam | cut -f11 |";
	while (<PIPE>) {
		my @is = (0 .. (length($_) - 2));
		$ontarget++;
		foreach my $i (@is) {
			$Qdat[$i][$Qcon{substr($_, $i, 1)}]++;
		}
	}

	open PIPE, "samtools view -XF0x004 -f0x010 -L $targetBed $bam | cut -f11 |";
	while (<PIPE>) {
		my @is = reverse(0 .. (length($_) - 2));
		$ontarget++;
		foreach my $i (@is) {
			$Qdat[$i][$Qcon{substr($_, $i, 1)}]++;
		}
	}

	open QUAL, ">$file.qual.tsv";
	print QUAL join("\t", "pos", 0 .. $#Qdat) . "\n";
	my $q0 = 0;
	my $q10 = 0;
	my $q20 = 0;
	my $q30 = 0;
	my $qtot = 0;
	foreach my $p (0 .. $#Qdat) {
		print QUAL join("\t", $p, map { $Qdat[$p][$_] || 0 }  (0 .. $#{$Qdat[$p]})) . "\n"; 
		foreach my $q (0 .. $#{$Qdat[$p]}) {
			$qtot += $Qdat[$p][$q] || 0;
			$q30 += $Qdat[$p][$q] || 0 if ($q >= 30);
			$q20 += $Qdat[$p][$q] || 0 if ($q >= 20 && $q < 30);
			$q10 += $Qdat[$p][$q] || 0 if ($q >= 10 && $q < 20);
			$q0 += $Qdat[$p][$q] || 0 if ($q < 10);
		}
	}	
	close QUAL;

	my $offtarget = $mapped - $ontarget;
	print join("\t", $file, $mapped, $ontarget, $offtarget, $unmapped, $q30/$qtot, $q20/$qtot, $q10/$qtot, $q0/$qtot) . "\n";
	
}		
