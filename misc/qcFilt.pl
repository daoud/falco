#!/usr/bin/perl

use strict;

my $qcQual = shift;
my $clinvar = shift;
my $cosmic = shift;
my $cosmicNC = shift;
my $output = shift;

my $minDp = shift || 100;
my $minVar = shift || 10;
my $minIns = shift || $minVar;
my $minDel = shift || $minVar;
my $minVarQ = shift || 100;
my $minInsQ = shift || 200;
my $minDelQ = shift || 200;

my %cvHsh = ();
print STDERR "Reading clinvars...\n";
open CV, "<$clinvar";
while (<CV>) {	last if (/#CHROM/); }
while (<CV>) {
	chomp;
	my @row = split(/\t/, $_);
	$cvHsh{"chr".$row[0].":".$row[1]} = [@row];
}
close CV;

print STDERR "Reading COSMIC...\n";
open CV, "<$cosmic";
while (<CV>) {	last if (/#CHROM/); }
while (<CV>) {
	chomp;
	my @row = split(/\t/, $_);
	$cvHsh{"chr".$row[0].":".$row[1]} = [@row];
}
close CV;

print STDERR "Reading COSMICNC...\n";
open CV, "<$cosmicNC";
while (<CV>) {	last if (/#CHROM/); }
while (<CV>) {
	chomp;
	my @row = split(/\t/, $_);
	$cvHsh{"chr".$row[0].":".$row[1]} = [@row];
}

close CV;
print STDERR "done!";

open QC, "<$qcQual";
my $head = readline QC;
chomp;
$head =~ s/\s+$//;
my @rhead = split(/\t/, $head);
my $coln = 0;
my %col = map { $_ => $coln++ } @rhead;
open OUT, ">$output.qc.ann.qual.filt.txt"; 
open CVOUT, ">$output.qc.ann.qual.clinvar.txt"; 
print OUT "$head\n";
print CVOUT "$head\n";

while (<QC>) {
	my $line = $_;
	my @row = split(/\t/, $_);
	my $key = $row[$col{"X.chr"}] . ":" . $row[$col{pos}];
	if (exists($cvHsh{$key})) {
		print CVOUT $line;
	}
	next if ($row[$col{dp}] < $minDp);
	# Var
	if ($row[$col{qScore}] >= $minVarQ && $row[$col{nVar}] > $minVar) {
		print OUT $line;		
	}
	# Ins
	if ($row[$col{qScoreI}] >= $minInsQ) {
		$row[$col{ins}] =~ /:(\d+)/;
		if ($1 >= $minIns) {
			print OUT $line;
		}
	}
	# Del
	if ($row[$col{qScoreD}] >= $minDelQ) {
		$row[$col{del}] =~ /:(\d+)/;
		if ($1 >= $minDel) {
			print OUT $line;
		}
	}
}

close OUT;
close CVOUT;
