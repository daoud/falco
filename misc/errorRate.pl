#!/usr/bin/perl -w
#

use strict;
my @files = @ARGV;
foreach my $file (@files) {
my $ref = 0;
my $var = 0;

my $file = shift;

open F, "<$file";
my $header = readline F; # Skip header
chomp $header;
my @hrow = split(/\t/, $header);
my $ci = 0;
my %col = ();
$col{$_} = $ci++ foreach (@hrow);


while (<F>) {
	chomp;
	my @row = split(/\t/, $_);
	#next if ($row[2] eq ".");
	#print join("\t", $row[6], $row[7]) . "\n"; sleep 1;
	my $ntRef = uc($row[4]);
	$ref += $row[$col{"n$ntRef"}];
	$var += $row[$col{"n$_"}] foreach (grep { $_ ne $ntRef  } qw/A C G T/);
}

close F;
my @path = split(/\//, $file);
my $rate = $var / ($ref + $var) * 100;
#print STDERR "$var/$ref -> $rate%\n";
print "$path[$#path]\t$var\t$ref\t$rate\n";
}
