#!/usr/bin/perl 

use strict;

# Read in path(s) and run bcl2fastq to fetch data into appropriate directories

my $runDir = shift;
my $project = shift || die "Project required";
my $baseDir = shift || "/gsdata/tmp/TSACP/$project";
my $bcl2fqbin = "/usr/local/bin/configureBclToFastq.pl";

if ( !-e $bcl2fqbin ) {
	print STDERR localtime() . " : $bcl2fqbin not found!\n";
	exit; 
}

#my $baseDir = shift || "/ccagc/data/SeqAnalysis/$project";
my $dry = 0;
my $outDir = undef;
(my $FCID = $runDir) =~ s/.*_(.*?)\/?$/$1/;

if (not defined($outDir)) {
	($outDir = $runDir) =~ s/.*\/(.*_.*_.*)_.*/$1/;
	$outDir = $baseDir ."/". $outDir;
}

print STDERR localtime() . " : PATH: " . $runDir . "\n";
print STDERR localtime() . " : BASEDIR: " . $baseDir . "\n";
print STDERR localtime() . " : OUTDIR: " . $outDir . "\n";
print STDERR localtime() . " : FCID: " . $FCID . "\n";

# open samplesheet
# Check run type and convert for bcl2fastq
my $col = 0;

my @headOut = qw/FCID Lane SampleID SampleRef Index Description Control Recipe Operator SampleProject/;
my $Lane = 1;
my $SampleRef = "human";
my $Control = "N";
my $SampleProject = $project;
my %header = ();
open SAMPLE, "<$runDir/SampleSheet.csv" or die;
while (<SAMPLE>) {
	chomp;
	s/\s+$//;
	if (/^(.*),(.*)$/) {
		$header{$1} = $2;
	}
	last if (/\[Data\]/);
}

if ($header{Assay} !~ /Amplicon/) {
	print STDERR localtime() . " : $header{Assay} : Wrong assay, exiting.\n";
	#exit;	
}

# Skipping project name check for now
#if ($header{"Project Name"} !~ /$project/) {
#	print STDERR localtime() . " : Wrong project, exiting.\n";
#	exit;
#}

my $Operator = $header{"Investigator Name"};
my $Recipe = $header{"Workflow"};

my $head = readline(SAMPLE);
chomp $head;
my %headIn = map { $_ => $col++ } split(/,/, $head);#"Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,I5_Index_ID,index2,Sample_Project,Description,Manifest,GenomeFolder");

my @samples = ();
my $nsam = 0;
while (<SAMPLE>) {
	chomp;
	my @row = split(/,/, $_);
	print STDERR localtime() . " : " . join(":", @row) . "\n";
	my $pass = 0;
	$nsam++ if ($row[$headIn{Sample_Project}] =~ /$SampleProject/);
	$pass++ if ($row[$headIn{Sample_Project}] =~ /$SampleProject/);
	$pass++ if ($row[$headIn{Sample_Project}] =~ /CONTROL/i);
	my $bc = substr($row[$headIn{index}],0,7) ."-". substr($row[$headIn{index2}],0,7);
	if ($pass == 0) {
		# Replace samplename with barcode
		$row[$headIn{Sample_ID}] = $bc;
	}
	push @samples, join(",", $FCID, $Lane, $row[$headIn{Sample_ID}], $SampleRef, $bc, $row[$headIn{Description}], $Control, $Recipe, $Operator, $SampleProject) . "\n";
}
close SAMPLE;

if ($dry == 1) {
	print STDERR localtime() . " : mkdir $outDir\n";
	print STDERR localtime() . " : " . join(",", @headOut) . "\n";
	print STDERR localtime() . " : " . join("", @samples);
}
elsif (-e $outDir) {
	print STDERR localtime() . " : Folder $outDir exists, exiting\n";
	exit;
}
elsif ($nsam > 0) {
	print STDERR localtime() . " : mkdir $outDir\n";
	system("mkdir -p $outDir");
	open SAMPLEOUT, ">$outDir/SampleSheet.csv";
	print SAMPLEOUT join(",", @headOut) . "\n";
	print STDERR localtime() . " : " . join(",", @headOut) . "\n";
	print SAMPLEOUT join("", @samples);
	print STDERR localtime() . " : " . join("", @samples);
	close SAMPLEOUT;
}
else {
	# Nothing to do exit!
	print STDERR localtime() . " : Nothing to do for $SampleProject, exiting!";
	exit;
}


# run configureBCL2fastq
my $bcl2fq = "$bcl2fqbin --no-eamss --input-dir $runDir/Data/Intensities/BaseCalls --output-dir $outDir/Unaligned --sample-sheet $outDir/SampleSheet.csv --mismatches 0 --fastq-cluster-count 0";
my $make = "cd $outDir/Unaligned && nohup make && touch bcl2fq.done && cd ..";

print STDERR localtime() . " : $bcl2fq\n";
print STDERR localtime() . " : $make\n";
if ($dry == 1) {
	exit;
}

my $pid1 = open BCL2FQ, "$bcl2fq 2>&1 |";
while (<BCL2FQ>) {
	print STDERR localtime() . " - $_";
}
close BCL2FQ;

my $pid2 = open MAKE, "$make 2>&1 |";
while (<MAKE>) {
	print STDERR localtime() . " - $_";
}
close MAKE;


#my $rc1 = 0; #system($bcl2fq);
# run make
#if ($rc1 == 0) {
#	my $rc2 = system($make);
#}
#else {
#	print STDERR localtime() . " : Error or warning encountered :$rc1\n";
#	print STDERR localtime() . " : Run make manually\n";
#	print STDERR localtime() . " : $make\n";
#}
