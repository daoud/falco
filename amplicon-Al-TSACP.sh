#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $DIR/init.sh

TMP=./tmp
if [ ! -e $TMP ]
then
	mkdir $TMP
fi

fq1=$1
fq2=$2
output=$3

bfq1=`basename $fq1`
bfq2=`basename $fq2`

if [ ! -e $TMP/$bfq1.trim ]
then
echo `date` " [$$] - $adapt_clipper -a $illmP5 $fq1 > $TMP/$bfq1.trim"
$adapt_clipper -a $illmP5 $fq1 > $TMP/$bfq1.trim
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$bfq1.qtrim ]
then
echo `date` " [$$] - $qTrim -t30 -i $TMP/$bfq1.trim -o $TMP/$bfq1.qtrim -Q34"
$qTrim -t30 -i $TMP/$bfq1.trim -o $TMP/$bfq1.qtrim -Q34
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$bfq2.trim ]
then
echo `date` " [$$] - $adapt_clipper -a $illmP7 $fq2 > $TMP/$bfq2.trim"
$adapt_clipper -a $illmP7 $fq2 > $TMP/$bfq2.trim
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$bfq2.qtrim ]
then
echo `date` " [$$] - $qTrim -t10 -i $TMP/$bfq2.trim -o $TMP/$bfq2.qtrim -Q34"
$qTrim -t30 -i $TMP/$bfq2.trim -o $TMP/$bfq2.qtrim -Q34
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$bfq1.qtrim.paired ]
then
echo `date` " [$$] - perl $pairUp $TMP/$bfq1.qtrim $TMP/$bfq2.qtrim"
perl $pairUp $TMP/$bfq1.qtrim $TMP/$bfq2.qtrim
echo `date` " [$$] - Done!"
fi

# FLASH Fastq joiner
if [ ! -e $TMP/$output.flashed.fq ]
then

$flash -O -r150 -f180 -c $TMP/$bfq1.qtrim.paired $TMP/$bfq2.qtrim.paired > $TMP/$output.flashed.fq

fi

#if [ ! -e $TMP/$output ] 
#then
#echo `date` " [$$] - $stampy -g $index -h $hash --readgroup=ID:$output,SM:$output --gatkcigarworkaround --bwaoptions=\"$bwaref\" --bwa="$bwa" --bwamark -o $TMP/$output -M $TMP/$bfq1.qtrim.paired $TMP/$bfq2.qtrim.paired"
#$stampy -g $index -h $hash --readgroup=ID:$output,SM:$output --gatkcigarworkaround --bwaoptions="$bwaref" --bwa="$bwa" --bwamark -o $TMP/$output -M $TMP/$bfq1.qtrim.paired $TMP/$bfq2.qtrim.paired
#echo `date` " [$$] - Done!"  
#fi

# Added -e 40 for maximum indel size of 40,...
#if [ ! -e $TMP/$output ] 
#then
#echo `date` " [$$] - $stampy -g $index -h $hash --readgroup=ID:$output,SM:$output --gatkcigarworkaround --bwaoptions=\"-e 40 $bwaref\" --bwa="$bwa" --bwamark -o $TMP/$output -M $TMP/$output.flashed.fq"
#$stampy -g $index -h $hash --readgroup=ID:$output,SM:$output --gatkcigarworkaround --bwaoptions="-e 40 $bwaref" --bwa="$bwa" --bwamark -o $TMP/$output -M $TMP/$output.flashed.fq
#echo `date` " [$$] - Done!"  
#fi

if [ ! -e $output.bam ]
then 
echo `date` " [$$] - $bwa aln -e r50 $bwaref $TMP/$output.flashed.fq > $TMP/$output.sai"
$bwa aln -e 50 $bwaref $TMP/$output.flashed.fq > $TMP/$output.sai
echo `date` " [$$] - Done!"

echo `date` " [$$] - $bwa samse -r \"@RG\\tID:$output\\tSM:$output\" $bwaref $TMP/$output.sai $TMP/$output.flashed.fq > $TMP/$output.sam"
$bwa samse -r "@RG\tID:$output\tSM:$output" $bwaref $TMP/$output.sai $TMP/$output.flashed.fq > $TMP/$output.sam
echo `date` " [$$] - Done!"

echo `date` " [$$] - $samtools view -uS $TMP/$output.sam | $samtools sort - $output"
$samtools view -uS $TMP/$output.sam | $samtools sort - $output
echo `date` " [$$] - Done!"

echo `date` " [$$] - $samtools index $output.bam"
$samtools index $output.bam
echo `date` " [$$] - Done!"
fi

echo `date` "[$$] - $VcAn $output.bam $output"
$VcAn $output.bam $output
echo `date` "[$$] - Done!"

echo `date` "[$$] - $report $output"
$report $output
echo `date` "[$$] - Done!"

echo `date` "[$$] - Cleaning up"
if [ ${#TMP} -ne 0 ]
then
	echo "rm -rf $TMP/$output*"
#	rm -rf $TMP/$output*
fi
echo `date` "[$$] - Done!"

exit
