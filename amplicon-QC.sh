#!/bin/bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TMP=./tmp
if [ ! -e $TMP ]
then   
        mkdir $TMP
fi

source $DIR/init.sh

#targetBed=$DIR/ref/manifests/TSACP/TSACP.bed
#samtools=$DIR/bin/samtools/samtools-0.1.18/samtools
#alignmentStats=$DIR/misc/alignmentStats.pl
#errorRate=$DIR/misc/errorRate.pl
#plotQc=$DIR/misc/plotsQC.R

#export PATH=$PATH:$DIR/bin/samtools/samtools-0.1.18/

if [ ! -e alignmentStats.tsv ]
then
$alignmentStats $targetBed *.bam > alignmentStats.tsv
fi

if [ ! -e errorStats.tsv ]
then
$errorRate *.qc.ann.txt > errorStats.tsv
fi

Rscript $plotQc alignmentStats.tsv errorStats.tsv


