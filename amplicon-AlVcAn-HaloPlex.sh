#!/bin/bash

index=/scratch/data/ref/hg19/stampy/hg19
hash=/scratch/data/ref/hg19/stampy/hg19
bwaref=/scratch/data/ref/hg19/bwa-5/hg19.fa

adapt_clipper=/opt/cutadapt/cutadapt-1.1/bin/cutadapt
illmP5="AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
illmP7="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT"
qTrim=/opt/bin/fastq_quality_trimmer
pairUp=/storage/shark1/data/Git/ampliconPipeLine/pairUp.pl
stampy=/opt/stampy/stampy-1.0.18/stampy.py
samtools=/opt/samtools/samtools-0.1.18/samtools
pileup2cov=/storage/shark1/data/Git/ampliconPipeLine/pileup2cov.pl
attachManifest=/storage/shark1/data/Git/ampliconPipeLine/attachGff.pl
manifest=/scratch/data/ref/manifests/HaloPlex/Analyzable-region-01355-1335878719.gff
snpEff=/opt/snpEff/snpEff_3_0a

plots=/storage/shark1/data/Git/ampliconPipeLine/plots.R

fq1=$1
fq2=$2
output=$3

if [ ! -e $fq1.trim ] 
then
echo `date` " [$$] - $adapt_clipper -a $illmP5 $fq1 > $fq1.trim"
$adapt_clipper -a $illmP5 $fq1 > $fq1.trim
echo `date` " [$$] - Done!"
fi

if [ ! -e $fq1.qtrim ]
then
echo `date` " [$$] - $qTrim -t30 -i $fq1.trim -o $fq1.qtrim -Q34"
$qTrim -t30 -i $fq1.trim -o $fq1.qtrim -Q34
echo `date` " [$$] - Done!"
fi

if [ ! -e $fq2.trim ]
then
echo `date` " [$$] - $adapt_clipper -a $illmP7 $fq2 > $fq2.trim"
$adapt_clipper -a $illmP7 $fq2 > $fq2.trim
echo `date` " [$$] - Done!"
fi

if [ ! -e $fq2.qtrim ]
then
echo `date` " [$$] - $qTrim -t30 -i $fq2.trim -o $fq2.qtrim -Q34"
$qTrim -t30 -i $fq2.trim -o $fq2.qtrim -Q34
echo `date` " [$$] - Done!"
fi

if [ ! -e $fq1.qtrim.paired ]
then
echo `date` " [$$] - perl $pairUp $fq1.qtrim $fq2.qtrim"
perl $pairUp $fq1.qtrim $fq2.qtrim
echo `date` " [$$] - Done!"
fi

if [ ! -e $output ]
then
echo `date` " [$$] - $stampy -g $index -h $hash --bwaoptions=\"$bwaref\" -o $output -M $fq1.qtrim.paired $fq2.qtrim.paired"
$stampy -g $index -h $hash --bwaoptions="$bwaref" -o $output -M $fq1.qtrim.paired $fq2.qtrim.paired
echo `date` " [$$] - Done!"  
fi

if [ ! -e $output.bam ]
then
echo `date` " [$$] - $samtools view -uS $output | $samtools sort - $output"
$samtools view -uS $output | $samtools sort - $output
echo `date` " [$$] - Done!"

echo `date` " [$$] - $samtools index $output.bam"
$samtools index $output.bam
echo `date` " [$$] - Done!"
fi

if [ ! -e $output.vcf ]
then
echo `date` " [$$] - $samtools mpileup -Euf $bwaref -L 1000000 -d 1000000 -F 0.01 -m 10 -o 20 $output.bam | bcftools view -bvcg - > $output.bcf"
$samtools mpileup -Euf $bwaref -L 1000000 -d 1000000 -F 0.01 -m 10 -o 20 $output.bam | bcftools view -bvcg - > $output.bcf
echo `date` " [$$] - Done!"
echo `date` " [$$] - bcftools view $output.bcf > $output.vcf"
bcftools view $output.bcf > $output.vcf
echo `date` " [$$] - Done!"
fi

if [ ! -e $output.txt ]
then
echo `date` " [$$] - java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o txt -c $snpEff/snpEff.config $output.vcf > $output.snpeff.vcf"
java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o txt -c $snpEff/snpEff.config $output.vcf > $output.snpeff.vcf
echo `date` " [$$] - Done!"

dbSnp=/scratch/data/ref/dbSNP/dbSnp_120616.vcf
dbNSFP=/scratch/data/ref/dbNSFP/dbNSFP2.0b3.txt
snpSift=/opt/SnpSift

echo `date` " [$$] [$$] - java -jar /opt/SnpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf"
java -jar /opt/SnpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf
echo `date` " [$$] [$$] - Done!"

echo `date` " [$$] [$$] - java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf"
java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf
echo `date` " [$$] [$$] - Done!"

fi

if [ ! -e $output.qc.txt ]
then
echo `date` " [$$] - $pileup2cov $bwaref $output.bam > $output.qc.txt"
$samtools view -Xq10 $output.bam | $pileup2cov $bwaref > $output.qc.txt
echo `date` " [$$] - Done!"
fi

if [ ! -e $output.qc.ann.txt ]
then
echo `date` " [$$] - $attachManifest $manifest $output.qc.txt $output.qc.targets.txt > $output.qc.ann.txt"
$attachManifest $manifest $output.qc.txt $output.qc.targets.txt > $output.qc.ann.txt
echo `date` " [$$] - Done!"
fi

if [ ! -e $output.pdf ]
then
echo `date` " [$$] - $plots $output.qc.ann.targets.txt $output.qc.targets.txt"
$plots $output.qc.ann.txt $output.qc.targets.txt $output.pdf
echo `date` " [$$] - Done!"
fi
