#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $DIR/init.sh

base=$1

vcf2tsv=$DIR/misc/vcf2tsv.pl
spliteff=$DIR/misc/splitEff.pl
addQc=$DIR/misc/addQc.pl
filterR=$DIR/misc/filter.R
filter=$DIR/misc/filter.pl
plotPng=$DIR/misc/plotsPng.R

# convert to tsv

echo `date` " [$$] - converting vcf to tsv" 
$vcf2tsv $base.samtools.vcf > $base.samtools.tsv
#$vcf2tsv $base.vs.snp.a.vcf > $base.vs.snp.a.tsv
$vcf2tsv $base.qc.ann.filt.a.vcf > $base.qc.ann.filt.a.tsv
$vcf2tsv $base.qc.ann.clinvar.a.vcf > $base.qc.ann.clinvar.a.tsv
#$vcf2tsv $base.qc.ann.2.a.vcf > $base.qc.ann.2.a.tsv

# split eff columns
echo `date` " [$$] - splitting vcf columns" 
$spliteff $base.samtools.tsv Samtools > $base.res.tsv
#$spliteff $base.vs.snp.a.tsv >> $base.res.tsv
$spliteff $base.qc.ann.filt.a.tsv Custom >> $base.res.tsv
$spliteff $base.qc.ann.clinvar.a.tsv Clinvar >> $base.res.tsv
#$spliteff $base.qc.ann.2.a.tsv Custom2 >> $base.res.tsv

# add count data
#$addQc $base.res.tmp $base.qc.ann.txt > $base.res.tsv

# Filter
echo `date` " [$$] - filtering data" 
#Rscript $filterR $base.res.tsv $base.qc.ann.txt 10 100 .95 $base
$filter $base.res.tsv $canonicals > $base.res.filtered.tsv

# Plot
echo `date` " [$$] - plotting data" 
Rscript $plotPng $base.qc.ann.qual.txt $base.qc2.ann.txt $base.qc.targets.txt $base.res.filtered.tsv $base.qc.ann.qual.clinvar.txt $locifilt $base
