#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TMP=./tmp
if [ ! -e $TMP ]
then
	mkdir $TMP
fi

index=$DIR/ref/hg19/stampy/hg19
hash=$DIR/ref/hg19/stampy/hg19
bwaref=$DIR/ref/hg19/bwa-5/hg19.fa
manifest=$DIR/ref/manifests/TSACP/TruSeq_Amplicon_Cancer_Panel_Manifest_AFP1_PN15032433.txt

illmP5="AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
illmP7="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT"

adapt_clipper=$DIR/bin/cutadapt/cutadapt-1.1/bin/cutadapt
qTrim=$DIR/bin/fastq_quality_trimmer
stampy=$DIR/bin/stampy/stampy-1.0.18/stampy.py
samtools=$DIR/bin/samtools/samtools-0.1.18/samtools
snpEff=$DIR/bin/snpEff/snpEff_3_0a
varscan=$DIR/bin/VarScan/VarScan.v2.2.11.jar
java=/usr/java/latest/bin/java
bwa=$DIR/bin/bwa/bwa-0.5.9/bwa

pairUp=$DIR/misc/pairUp.pl
pileup2cov=$DIR/misc/pileup2cov.pl
attachManifest=$DIR/misc/attachManifest.pl
plots=$DIR/misc/plots.R
VcAn=$DIR/amplicon-VcAn-TSACP.sh
report=$DIR/amplicon-Report.sh

fq1=$1
fq2=$2
output=$3

bfq1=`basename $fq1`
bfq2=`basename $fq2`

if [ ! -e $TMP/$bfq1.trim ]
then
echo `date` " [$$] - $adapt_clipper -a $illmP5 $fq1 > $TMP/$bfq1.trim"
$adapt_clipper -a $illmP5 $fq1 > $TMP/$bfq1.trim
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$bfq1.qtrim ]
then
echo `date` " [$$] - $qTrim -t30 -i $TMP/$bfq1.trim -o $TMP/$bfq1.qtrim -Q34"
$qTrim -t30 -i $TMP/$bfq1.trim -o $TMP/$bfq1.qtrim -Q34
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$bfq2.trim ]
then
echo `date` " [$$] - $adapt_clipper -a $illmP7 $fq2 > $TMP/$bfq2.trim"
$adapt_clipper -a $illmP7 $fq2 > $TMP/$bfq2.trim
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$bfq2.qtrim ]
then
echo `date` " [$$] - $qTrim -t10 -i $TMP/$bfq2.trim -o $TMP/$bfq2.qtrim -Q34"
$qTrim -t30 -i $TMP/$bfq2.trim -o $TMP/$bfq2.qtrim -Q34
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$bfq1.qtrim.paired ]
then
echo `date` " [$$] - perl $pairUp $TMP/$bfq1.qtrim $TMP/$bfq2.qtrim"
perl $pairUp $TMP/$bfq1.qtrim $TMP/$bfq2.qtrim
echo `date` " [$$] - Done!"
fi

if [ ! -e $TMP/$output ] 
then
echo `date` " [$$] - $stampy -g $index -h $hash --readgroup=ID:$output,SM:$output --gatkcigarworkaround --bwaoptions=\"$bwaref\" --bwa="$bwa" --bwamark -o $TMP/$output -M $TMP/$bfq1.qtrim.paired $TMP/$bfq2.qtrim.paired"
$stampy -g $index -h $hash --readgroup=ID:$output,SM:$output --gatkcigarworkaround --bwaoptions="$bwaref" --bwa="$bwa" --bwamark -o $TMP/$output -M $TMP/$bfq1.qtrim.paired $TMP/$bfq2.qtrim.paired
echo `date` " [$$] - Done!"  
fi

if [ ! -e $output.bam ] 
then
echo `date` " [$$] - $samtools view -uS $TMP/$output | $samtools sort - $output"
$samtools view -uS $TMP/$output | $samtools sort - $output
echo `date` " [$$] - Done!"

echo `date` " [$$] - $samtools index $output.bam"
$samtools index $output.bam
echo `date` " [$$] - Done!"
fi

echo `date` "[$$] - $VcAn $output.bam $output"
$VcAn $output.bam $output
echo `date` "[$$] - Done!"

echo `date` "[$$] - $report $output"
$report $output
echo `date` "[$$] - Done!"

exit
if [ ! -e $output.vcf ] 
then
# Use Extended BAQ!
# Tweak indel settings -m and -F
echo `date` " [$$] - $samtools mpileup -Euf $bwaref -L 1000000 -d 1000000 -m 150 -F 0.01 -o 20 $output.bam | bcftools view -bvcg - > $output.bcf"
$samtools mpileup -Euf $bwaref -L 1000000 -d 1000000 -m 150 -F 0.01 -o 20 $output.bam | bcftools view -bvcg - > $output.bcf
echo `date` " [$$] - Done!"
echo `date` " [$$] - bcftools view $output.bcf > $output.vcf"
bcftools view $output.bcf > $output.vcf
echo `date` " [$$] - Done!"
fi

if [ ! -e $output.vs.snp.vcf ]
then
# VarScan seems a better alternative for smaller VAF's Samtools seems to obscure some false negatives....
echo `date` " [$$] - $samtools mpileup -Bf $bwaref $output.bam | $java -jar $varscan mpileup2snp --min-var-freq 0.05 --output-vcf > $output.vs.snp.vcf"
$samtools mpileup -Bf $bwaref $output.bam | $java -jar $varscan mpileup2snp --min-var-freq 0.05 --output-vcf > $output.vs.snp.vcf
echo `date` " [$$] - Done!"
fi

if [ ! -e $output.vs.snp.tsv ]
then
# VarScan seems a better alternative for smaller VAF's Samtools seems to obscure some false negatives....
echo `date` " [$$] - $samtools mpileup -Bf $bwaref $output.bam | $java -jar $varscan mpileup2snp --min-var-freq 0.05 > $output.vs.snp.tsv"
$samtools mpileup -Bf $bwaref $output.bam | $java -jar $varscan mpileup2snp --min-var-freq 0.05 > $output.vs.snp.tsv
echo `date` " [$$] - Done!"
fi

if [ ! -e $output.vs.indel.vcf ]  
then
echo `date` " [$$] - $samtools mpileup -Bf $bwaref $output.bam | $java -jar $varscan mpileup2indel --min-var-freq 0.05 --output-vcf > $output.vs.indel.vcf"
$samtools mpileup -Bf $bwaref $output.bam | $java -jar $varscan mpileup2indel --min-var-freq 0.05 --output-vcf > $output.vs.indel.vcf
echo `date` " [$$] - Done!"
fi 

# Annotation
dbSnp=/scratch/data/ref/dbSNP/dbSnp_120616.vcf
dbNSFP=/scratch/data/ref/dbNSFP/dbNSFP2.0b3.txt
snpSift=/opt/SnpSift

VcfIn=$output.vcf
VcfOut=$output.samtools.vcf

if [ ! -e $VcfOut ]
then
# Samtools annotation
echo `date` " [$$] - $java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > tmp.snpeff.vcf"
$java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > $output.snpeff.vcf
mv $output.snpeff.vcf $VcfOut
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf
#mv $output.snpeff.3.vcf $VcfOut
echo `date` " [$$] - Done!"
fi 

VcfIn=$output.vs.snp.vcf
VcfOut=$output.vs.snp.a.vcf

if [ ! -e $VcfOut ]
then
# VarScan SNP annotation
echo `date` " [$$] - $java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > tmp.snpeff.vcf"
$java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > $output.snpeff.vcf
mv $output.snpeff.vcf $VcfOut
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf
#mv $output.snpeff.3.vcf $VcfOut
echo `date` " [$$] - Done!"
fi

VcfIn=$output.vs.indel.vcf
VcfOut=$output.vs.indel.a.vcf

if [ ! -e $VcfOut ]
then
# VarScan INDEL annotation
echo `date` " [$$] - $java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > tmp.snpeff.vcf"
$java -jar $snpEff/snpEff.jar eff hg19 -noStats -noLog -o vcf -c $snpEff/snpEff.config $VcfIn > $output.snpeff.vcf
mv $output.snpeff.vcf $VcfOut
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar annotate -v $dbSnp $output.snpeff.vcf > $output.snpeff.2.vcf
echo `date` " [$$] - Done!"

echo `date` " [$$] - $java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf"
#$java -jar /opt/SnpSift/SnpSift_latest.jar dbnsfp -v $dbNSFP $output.snpeff.2.vcf > $output.snpeff.3.vcf
#mv $output.snpeff.3.vcf $VcfOut
echo `date` " [$$] - Done!"
fi


if [ ! -e $output.qc.txt ]
then
echo `date` " [$$] - $pileup2cov $bwaref $output.bam > $output.qc.txt"
$samtools view -Xq10 $output.bam | $pileup2cov $bwaref > $output.qc.txt
echo `date` " [$$] - Done!"

echo `date` " [$$] - $samtools view -Xq10 -f0x0040 $output.bam | $pileup2cov $bwaref $output.bam > $output.R1.qc.txt"
$samtools view -Xq10 -f0x0040 $output.bam | $pileup2cov $bwaref > $output.R1.qc.txt
echo `date` " [$$] - Done!"

echo `date` " [$$] - $samtools view -Xq10 -f0x0080 $output.bam | $pileup2cov $bwaref $output.bam > $output.R2.qc.txt"
$samtools view -Xq10 -f0x0080 $output.bam | $pileup2cov $bwaref > $output.R2.qc.txt
echo `date` " [$$] - Done!"

echo `date` " [$$] - $attachManifest $manifest $output.qc.txt $output.qc.targets.txt > $output.qc.ann.txt"
$attachManifest $manifest $output.qc.txt $output.qc.targets.txt > $output.qc.ann.txt
echo `date` " [$$] - Done!"

echo `date` " [$$] - $attachManifest $manifest $output.R1.qc.txt $output.R1.qc.targets.txt > $output.R1.qc.ann.txt"
$attachManifest $manifest $output.R1.qc.txt $output.R1.qc.targets.txt > $output.R1.qc.ann.txt
echo `date` " [$$] - Done!"

echo `date` " [$$] - $attachManifest $manifest $output.R2.qc.txt $output.R2.qc.targets.txt > $output.R2.qc.ann.txt"
$attachManifest $manifest $output.R2.qc.txt $output.R2.qc.targets.txt > $output.R2.qc.ann.txt
echo `date` " [$$] - Done!"

echo `date` " [$$] - $plots $output.qc.ann.targets.txt $output.qc.targets.txt"
$plots $output.qc.ann.txt $output.qc.targets.txt $output.pdf
echo `date` " [$$] - Done!"
fi
